package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println("Input Player name: "); // gets player name
        Scanner scanner = new Scanner(System.in);
        String playerName = scanner.nextLine();
        System.out.println("Welcome " + playerName + "!");

        String[] cardName = new String[6]; // card names
        cardName[0] = "Lee Sin";
        cardName[1] = "Aurelion Sol";
        cardName[2] = "Jhin";
        cardName[3] = "Pyke";
        cardName[4] = "Camille";
        cardName[5] = "Rengar";

        CardStack deck = new CardStack(30); // create deck
        CardStack discardedPile = new CardStack(30); // create discarded pile
        List<Card> playerHand = new ArrayList<>(30); // create hand
        int cardsOnDeck = 0;
        int cardsOnPile = 0;

        for (int i = 0; i < 30; i++) // fill deck with random cards
        {
            int num = randomize(0, 5);
            deck.push(new Card(cardName[num]));
            cardsOnDeck++;
        }

//        deck.printStack(); // print deck; for checking

        while (cardsOnDeck > 0)
        {
            System.out.println("\n\nPlayer: " + playerName); // display player name
            System.out.println("\nChoosing action...");
            int randomAction = randomize(1, 3); // random action

            if (randomAction == 1) // Draw from deck; pop deck + push hand
            {
                int numOfCards = randomize(1, 5);
                System.out.println("\nDraw " + numOfCards + " card/s from your deck!");
                for (int i = 0; i < numOfCards; i++)
                {
                    if (cardsOnDeck == 0) // to avoid exceeding draws
                    {
                        continue;
                    }

                    else {
                        Card drawnCard = deck.pop();
                        playerHand.add(drawnCard);
                        cardsOnDeck--;
                    }
                }
            }

            else if (randomAction == 2) // Discard; pop hand + push pile
            {
                int numOfCards = randomize(1, 5);
                System.out.println("\nDiscard " + numOfCards + " card/s from your hand!");
                for (int i = 0; i < numOfCards; i++)
                {
                    if (playerHand.size() == 0) // to skip if no cards on hand
                    {
                        continue;
                    }
                    else {
                        Card discardedCard = playerHand.get(playerHand.size() - 1); // store the card to be discarded
                        playerHand.remove(playerHand.size() - 1); // remove the card
                        discardedPile.push(discardedCard); // add the card to the pile
                        cardsOnPile++;
                    }
                }
            }

            else if (randomAction == 3) // Draw from pile; pop pile + push hand
            {
                int numOfCards = randomize(1, 5);
                System.out.println("\nAdd " + numOfCards + " card/s to your hand from your graveyard!");
                for (int i = 0; i < numOfCards; i++)
                {
                    if (cardsOnPile == 0) // to avoid exceeding draws
                    {
                        continue;
                    }

                    else {
                        Card drawnCard = discardedPile.pop();
                        playerHand.add(drawnCard);
                        cardsOnPile--;
                    }
                }
            }

            System.out.println("\nCards on hand [" + playerHand.size() + "]: ");
            for (Card c : playerHand) // print hand
            {
                System.out.println(c);
            }

            System.out.println("\nNumber of remaining cards on deck: " + cardsOnDeck);
            System.out.println("Number of cards on the discarded pile: " + cardsOnPile);
            }

        System.out.println("\nGAME OVER.");
        }

    public static int randomize(int min, int max) // get a random number
    {
        Random num = new Random();
        return num.nextInt(max - min) + min;
    }

}
