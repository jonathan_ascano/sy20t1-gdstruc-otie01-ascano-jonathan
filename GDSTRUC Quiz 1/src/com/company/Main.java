package com.company;

public class Main {

    public static void main(String[] args) {
	    int[] numbers = new int[10];

        numbers[0] = 89;
	    numbers[1] = 12;
        numbers[2] = 25;
        numbers[3] = -143;
        numbers[4] = 5;
        numbers[5] = 90;
        numbers[6] = 103;
        numbers[7] = 46;
        numbers[8] = 34;
        numbers[9] = 4;

        System.out.println("Before Sorting:");
        printArray(numbers);

        bubbleSort(numbers);
        System.out.println("\n\nAfter Bubble Sort:");
	    printArray(numbers);

        selectionSort(numbers);
        System.out.println("\n\nAfter Selection Sort:");
        printArray(numbers);
    }

    private static void bubbleSort(int[] arr) // Bubble Sort in descending order
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            for (int i = 0; i < lastSortedIndex; i++)
            {
                if (arr[i] < arr[i+ 1])
                {
                    int temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                }
            }
        }
    }

    private static void selectionSort(int[] arr) // Selection Sort in descending order; looking for the smallest first
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            int smallestIndex = 0;

            for (int i = 1; i <= lastSortedIndex; i++)
            {
                if (arr[i] < arr[smallestIndex])
                {
                    smallestIndex = i;
                }
            }

            int temp = arr[lastSortedIndex];
            arr[lastSortedIndex] = arr[smallestIndex];
            arr[smallestIndex] = temp;
        }
    }

    private static void printArray(int[] arr) // print array
    {
        for (int j : arr)
        {
            System.out.print(j + " ");
        }
    }
}
