package com.company;

public class PlayerLinkedList {
    private PlayerNode head;
    private Player player;

    public void addToFront(Player player)
    {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
    }

    public void printList()
    {
        PlayerNode current = head;
        System.out.print("HEAD -> ");
        while (current != null)
        {
            System.out.print(current);
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.println("null");
    }

    public void removeFirstElement() // 1. Remove first element
    {
        this.head = head.getNextPlayer();
    }

    public int countElements() // 2. Size counter
    {
        PlayerNode current = head;
        int counter = 0;
        while (current != null)
        {
            current = current.getNextPlayer();
            counter++;
        }
        return counter;
    }

    // I'm unable to do #3 :(
}
