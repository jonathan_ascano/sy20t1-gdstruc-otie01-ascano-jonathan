package com.company;

import java.util.Random;

public class Main {

    public static void main(String[] args) {

        ArrayQueue queue = new ArrayQueue(5);
        int game = 1;

        while (game < 11) { // game
            System.out.println("Game " + game);
            System.out.println("===========\n");

            while (queue.size() < 5) { // turn
                Random random = new Random();
                int upperBound = 7;
                int intRandom = random.nextInt(upperBound); // randomized player count 1-7

                for (int i = 0; i == intRandom; i++) {
                    queue.add(new Player(1, "Player", 50));
                }

                if (queue.size() > 5) // check if queue size exceeds 5
                {
                    int removeCount = queue.size() - 5;
                    for (int i = 0; i < removeCount; i++) // delete excess
                    {
                        queue.remove();
                    }
                }
            }
            queue.printQueue(); // for checking
            System.out.println("Player count: " + queue.size()); // display player count
            System.out.println("=================\n");

            for (int i = 0; i < 5; i++) { // pop the queue
                queue.remove();
            }
            game++;
        }

        System.out.println("Game Over."); // ends at Game 10

    }
}
