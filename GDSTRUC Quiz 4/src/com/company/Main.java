package com.company;

public class Main {

    public static void main(String[] args) {

        Player biHan = new Player(543, "SubZero", 100);
        Player hasashi = new Player(544, "Scorpion", 100);
        Player lk = new Player(540, "Liu Kang", 90);
        Player sandman = new Player (560, "Kabal", 88);
        Player johnny = new Player(566, "Johnny Cage", 90);

        SimpleHashtable hashtable = new SimpleHashtable();
        hashtable.put(biHan.getUserName(), biHan);
        hashtable.put(hasashi.getUserName(), hasashi);
        hashtable.put(lk.getUserName(), lk);
        hashtable.put(sandman.getUserName(), sandman);
        hashtable.put(johnny.getUserName(), johnny);

        hashtable.printHashtable();

        hashtable.remove("SubZero");
        System.out.println("\nAfter removing: ");
        hashtable.printHashtable();
    }
}
