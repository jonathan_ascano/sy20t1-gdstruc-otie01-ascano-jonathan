package com.company;

public class Main {

    public static void main(String[] args) {
        Tree tree = new Tree();

        tree.insert(43);
        tree.insert(23);
        tree.insert(12);
        tree.insert(32);
        tree.insert(1);
        tree.insert(5);
        tree.insert(54);
        tree.insert(-4);

        tree.traverseInOrderDescending();
        //System.out.println(tree.get(1));

    }
}
